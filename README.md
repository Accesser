# The project has been moved to <https://codeberg.org/antigng/Accesser>

# Accesser

*forked from [github.com/URenko/Accesser](https://github.com/URenko/Accesser)*

A tool for bypassing SNI-based HTTPS Filtering using [Domain Fronting](https://en.wikipedia.org/wiki/Domain_fronting)

[Supported sites](https://ygsk10.gitlab.io/notyet/ac.yaml)


## Explanation

Accesser tries TLS handshake without sending SNI; Or you can add rules in [config.toml](config.toml)'s `alter_hostname`, ordering Accesser use specified SNI for specified site. In both scenario Accesser will check the certificates is correct for that SNI, print a WARNING if not match, and will abort the connection.

Accesser would also abort connection for expired or self-signed certificates.

## Usage
### If you don't know what Python is
Download Windows executable program from [here](https://github.com/URenko/Accesser/releases/download/v0.8.0/accesser.exe) and run it. The first time you use it, you will be asked to install a certificate, just select yes.
### If Python 3.10\* or later is already installed
```
pip3 install -U accesser[doh]
```

If you don't need DNS-over-HTTPS, you can install without `[doh]`.
Then launch it with the following command:

```
accesser
```

For Windows, by default (without specifying `--notsetproxy`) it will set the system PAC proxy to `http://localhost:7654/pac/?t=<random number>`, if not you can set it manually.

In addition, for Windows, by default (without specifying `--notimportca`) the certificate will be imported to the system automatically, if not you can import it manually, please see [here](https://github.com/URenko/Accesser/wiki/FAQ#q-windows%E8%AE%BF%E9%97%AE%E7%9B%B8%E5%85%B3%E7%BD%91%E7%AB%99%E5%87%BA%E7%8E%B0%E8%AF%81%E4%B9%A6%E9%94%99%E8%AF%AF%E6%82%A8%E7%9A%84%E8%BF%9E%E6%8E%A5%E4%B8%8D%E6%98%AF%E7%A7%81%E5%AF%86%E8%BF%9E%E6%8E%A5neterr_cert_invalid%E4%B9%8B%E7%B1%BB%E7%9A%84%E6%80%8E%E4%B9%88%E5%8A%9E%E8%AF%81%E4%B9%A6%E5%AF%BC%E5%85%A5%E9%94%99%E8%AF%AF%E6%80%8E%E4%B9%88%E5%8A%9E%E5%A6%82%E4%BD%95%E5%8D%B8%E8%BD%BD%E8%AF%81%E4%B9%A6).

## Configuration
Edit `config.toml` in the working directory. See the comments therein. After saving, reopen the program.

## Notes for CDNs

see [CDN.md](/CDN.md).
