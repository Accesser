# Notes for CDNs

## Akamai

Support TLS handshake without SNI, and does not require SNI extension and HTTP Host header to contain the same domain

## Amazon CloudFront


It does not support TLS handshake without SNI

You must specific correct SNI in `config.json`

### Example:

For `www.wsj.com` 
Run

$ `nali-dig @208.67.222.222 -p 5353 -t CNAME www.wsj.com +short`

```
dlp0y1mxy0v3u.cloudfront.net. [AWS CloudFront]
```

Or run (If MS Windows)

\> `nali-nslookup -q=cname -port=5353 www.wsj.com 208.67.222.222`

```
Server:         208.67.222.222 [美国 加利福尼亚州旧金山 市 OpenDNS 有限公司公众 DNS 服务器]
Address:        208.67.222.222 [美国 加利福尼亚州旧金山 市 OpenDNS 有限公司公众 DNS 服务器]#5353

Non-authoritative answer:
www.wsj.com     canonical name = dlp0y1mxy0v3u.cloudfront.net. [AWS CloudFront]

Authoritative answers can be found from:
```

Add below in `config.json`'s `alert_hostname` field

```
"www.wsj.com": "dlp0y1mxy0v3u.cloudfront.net"
```

## Automattic

Support TLS handshake without SNI, and does not require SNI extension and HTTP Host header to contain the same domain

Note: this IP `192.0.66.2` has been blocked using QoS filtering

Other IPs in the range `192.0.66.0/24` is not

## CloudFlare

By default(most sites), it does not support TLS handshake without SNI, and will check if SNI extension and HTTP Host header contains the same domain

Domain Fronting is not possible

## ddos-guard.net

Support TLS handshake without SNI, and does not require SNI extension and HTTP Host header to contain the same domain

I suggest that you use `ddos-guard.net` as the set SNI, other values will [return](https://github.com/URenko/Accesser/pull/93) strange self-signed TLS certificates

## Fastly

Support TLS handshake without SNI, and does not require SNI extension and HTTP Host header to contain the same domain

## Google Web Service

SNI is required, but it doesn't require SNI and Host to contain same domain.

e.g.

`curl -v https://about.google --connect-to ::142.250.98.90 -H "Host: www.google.com.hk"`

is OK.

## Incapsula

Support TLS handshake without SNI, and does not require SNI extension and HTTP Host header to contain the same domain

## StackPath

Support TLS handshake without SNI, and does not require SNI extension and HTTP Host header to contain the same domain

Note: this IP `151.139.128.11` has been blocked using QoS filtering

Other IPs in the range `151.139.128.0/24` is not